import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AdminComponent } from './admin/admin.component';
import { Error404Component } from './error404/error404.component';
import { InternComponent } from './intern/intern.component';
import { ManagementComponent } from './management/management.component';

const routes: Routes = [
  {path: 'accueil', component: AccueilComponent },
  {path: 'intern', component: InternComponent },
  {path: 'admin', component: AdminComponent },
  {path: 'management', component: ManagementComponent },
  {path: 'error404', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
