import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import {
 faPenToSquare,
 faPaperPlane
} from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.css']
})
export class InternComponent implements OnInit{
  
  //icon
  faPenToSquare = faPenToSquare;
  faPaperPlane = faPaperPlane;
  
  //var
  public id: number;
  public role: string;
  public nom: string;
  public prenom: string;
  public email: string;
  public tel: string;
  public genre: string;
  public chambre: string;
  public code_wifi: string;
  public session: string;
  public periode_de_formation: string;
  public status_cle: boolean;
  public imatriculation: string;
  public contact_durgence: string;
  public avatar_url: string;
  public avatar_alt: string;

  //tableaux
  public signalementTab: [{ "objet":string, "date":string, "note":string, "statut":string, }];
  public activiterTab: [{ "activite":string, "date":string, "information":string, "disponibiliter":string, "disponibiliter_max":string, }];
  public jeux_societerTab: [{ "nom":string, "quantiter":string, }];

  constructor(private http: HttpClient){

    //init var
    this.id = 0;
    this.role = "";
    this.nom = "";
    this.prenom = "";
    this.email = "";
    this.tel = "";
    this.genre = "";
    this.chambre = "";
    this.code_wifi = "";
    this.session = "";
    this.periode_de_formation = "";
    this.status_cle = false;
    this.imatriculation = "";
    this.contact_durgence = "";
    this.avatar_url = "";
    this.avatar_alt = "";
    
    //init tab
    this.signalementTab = [{ "objet":"", "date":"", "note":"", "statut":"", }];
    this.activiterTab = [{ "activite": "", "date": "", "information": "", "disponibiliter": "", "disponibiliter_max": "", }];
    this.jeux_societerTab = [{ "nom":"", "quantiter":"", }];
    
  }

  ngOnInit(): void {
    this.LoadData();
  }

  LoadData(){
    return this.http.get("assets/data/data.json")
    .subscribe((data) =>{
      let jsonObj = Object.create(data);

      //var
      this.role = jsonObj["utilisateur"][0]["role"];
      this.nom = jsonObj["utilisateur"][0]["nom"];
      this.prenom = jsonObj["utilisateur"][0]["prenom"];
      this.email = jsonObj["utilisateur"][0]["email"];
      this.tel = jsonObj["utilisateur"][0]["tel"];
      this.genre = jsonObj["utilisateur"][0]["genre"];
      this.chambre = jsonObj["utilisateur"][0]["chambre"];
      this.code_wifi = jsonObj["utilisateur"][0]["code_wifi"];
      this.session = jsonObj["utilisateur"][0]["session"];
      this.periode_de_formation = jsonObj["utilisateur"][0]["periode_de_formation"];
      this.status_cle = jsonObj["utilisateur"][0]["status_cle"];
      this.imatriculation = jsonObj["utilisateur"][0]["imatriculation"];
      this.contact_durgence = jsonObj["utilisateur"][0]["contact_durgence"];
      this.avatar_url = jsonObj["utilisateur"][0]["avatar_url"];
      this.avatar_alt = jsonObj["utilisateur"][0]["avatar_alt"];

      //tab
      this.signalementTab = jsonObj["utilisateur"][0]["signalement"];
      this.activiterTab = jsonObj["liste_des_activiter"];
      this.jeux_societerTab = jsonObj["liste_jeux_societer"];
    });
  }

}
